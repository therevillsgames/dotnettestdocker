image: alpine:latest

variables:
  TRACE: "true"
  DEVOPS_DOMAIN: ""
  OPENSHIFT_SERVER: ""
  DOCKER_DRIVER: overlay2
  INFRA_CONTAINER_REGISTRY: ""
  APP_NAME: "dotnettest"
  PREFIX_NAMESPACE: ""
  NAMESPACE: ""
  KUBERNETES_VERSION: 1.10.9
  PORT: 5000

stages:
  - build
  - deploy

build:
  stage: build
  image: docker:stable-git
  services:
    - docker:stable-dind
  script:
    - setup_docker
    - build
    - push_build
  only:
    - branches

deploy:
  stage: deploy
  script:
    - install_dependencies
    - ensure_namespace
    - create_secret
    - mkdir -p .generated
    - cp templates/*.yaml ./.generated/
    - sed -i "s/<APP_NAME>/${APP_NAME}/" ./.generated/deployment.yaml
    - sed -i "s~<IMAGE>~${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}~" ./.generated/deployment.yaml
    - sed -i "s/<PORT>/${PORT}/" ./.generated/deployment.yaml
    - sed -i "s/<SECRET_REGISTRY_KEY>/${SECRET_REGISTRY_KEY}/" ./.generated/deployment.yaml
    - cat ./.generated/deployment.yaml
    - echo ""
    - sed -i "s/<APP_NAME>/${APP_NAME}/" ./.generated/service.yaml
    - sed -i "s/<PORT>/${PORT}/" ./.generated/service.yaml
    - cat ./.generated/service.yaml
    - echo ""
    - kubectl apply -f ./.generated -n ${NAMESPACE}
    - kubectl rollout status -n "$NAMESPACE" -w "deployment/${APP_NAME}-deployment"
  environment: staging
  only:
    refs:
      - branches


.setup_scripts: &setup_scripts |
  [[ "$TRACE" ]] && set -x
  export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
  export CI_APPLICATION_TAG=$CI_COMMIT_SHA
  export SECRET_REGISTRY_KEY=gitlab-registry
  
  # set up the docker host
  function setup_docker() {
    echo "DOCKER_HOST = $DOCKER_HOST"
    echo "KUBERNETES_PORT = $KUBERNETES_PORT"
    if ! docker info &>/dev/null; then
      if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
      fi
    fi
  }

  # log onto the container registry
  function registry_login() {
    if [[ -n "$CI_REGISTRY_USER" ]]; then
      echo "Logging to GitLab Container Registry with CI credentials..."
      docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
      echo ""
    fi
  }

  # build the docker image
  function build() {
    registry_login

    echo "Building Dockerfile-based application..."
    echo "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
    docker build --network host -t "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG" .
  }

  # push container image to registry
  function push_build() {
    registry_login
    echo "Pushing to GitLab Container Registry... $CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
    docker push "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
    echo ""
  }

  # download K8s
  function install_dependencies() {
    echo "install_dependencies"
    apk add -U openssl curl tar gzip bash ca-certificates git
    curl -L -o /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
    curl -L -O https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk
    apk add glibc-2.28-r0.apk
    rm glibc-2.28-r0.apk

    curl -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    chmod +x /usr/bin/kubectl
    kubectl version --client

    KUBE_CLUSTER_NAME="idp-d4e-ocp"
    
    kubectl config set-cluster $KUBE_CLUSTER_NAME --server="$OPENSHIFT_SERVER"
    kubectl config set clusters.$KUBE_CLUSTER_NAME.certificate-authority-data ${CERTIFICATE_AUTHORITY}
    kubectl config set-credentials admin --token=${SERVICE_ACCOUNT_TOKEN}
    kubectl config set-context default --cluster=$KUBE_CLUSTER_NAME --user=admin
    kubectl config use-context default

    kubectl config view

    kubectl cluster-info

  }

  # create the namespace
  function ensure_namespace() {
    NAMESPACE=${PREFIX_NAMESPACE}-${CI_ENVIRONMENT_SLUG}
    echo "Checking ${NAMESPACE}"
    kubectl describe namespace "${NAMESPACE}" || kubectl create namespace "${NAMESPACE}"
  }

  # supply creds to K8s to be able to grab the images etc
  function create_secret() {
    echo "Create secret..."
    if [[ "$CI_PROJECT_VISIBILITY" == "public" ]]; then
      return
    fi

    echo "Calling kubectl create secret.."

    kubectl create secret -n "$NAMESPACE" \
      docker-registry $SECRET_REGISTRY_KEY \
      --docker-server="$CI_REGISTRY" \
      --docker-username="${CI_DEPLOY_USER:-$CI_REGISTRY_USER}" \
      --docker-password="${CI_DEPLOY_PASSWORD:-$CI_REGISTRY_PASSWORD}" \
      --docker-email="$GITLAB_USER_EMAIL" \
      -o yaml --dry-run | kubectl replace -n "$NAMESPACE" --force -f -
  }



before_script:
  - *setup_scripts
