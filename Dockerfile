FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine AS build
WORKDIR /app

# copy everything else and build app
COPY testapp/. ./testapp/
WORKDIR /app/testapp
RUN dotnet publish -c Release -o out


FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine AS runtime
ENV ASPNETCORE_URLS=http://*:5000
WORKDIR /app
COPY --from=build /app/testapp/out ./
ENTRYPOINT ["dotnet", "csharpweb.dll"]